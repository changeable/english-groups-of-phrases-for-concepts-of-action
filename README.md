# English small common phrases for communication

**Project for small (length) common phrases, of common core words, to help with communicating.**

Project is marked with the *CC0 1.0 Universal* public domain dedication.


Some infinitive verb - ("to") _action word or being word_ - concepts for phrases:
- possible... for an action, to have it done - or be able to take place
- have... some -thing (feeling, activity or emotion) in a person's memory
  - emotions and needs
  - time for (or with) some -thing, -one
- do... an action or activity
  - say... (something)
  - make different... (a thing)
  - behaviours... done for (about) an action